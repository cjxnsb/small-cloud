package cn.cjxnsb.core.service;

import cn.cjxnsb.common.domain.SystemStatus;

public interface DemoService {

    SystemStatus getSystemStatus();

}
