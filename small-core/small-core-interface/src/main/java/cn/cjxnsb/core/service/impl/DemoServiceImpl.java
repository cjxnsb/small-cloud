package cn.cjxnsb.core.service.impl;


import cn.cjxnsb.common.domain.SystemStatus;
import cn.cjxnsb.core.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DemoServiceImpl implements DemoService {

    @Autowired
    private SystemStatus systemStatus;

    @Override
    public SystemStatus getSystemStatus() {
        return systemStatus;
    }
}
