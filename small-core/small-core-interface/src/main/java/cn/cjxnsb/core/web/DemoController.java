package cn.cjxnsb.core.web;

import cn.cjxnsb.core.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    private DemoService demoService;

    @Value("${provider.initStatus}")
    private String initStatus;

    @GetMapping("/initStatus")
    public String getInitStatus() {
        return demoService.getSystemStatus().getInitStatus();
    }

    @GetMapping("/directInitStatus")
    public String getDirectInitStatus() {
        return initStatus;
    }

}
