package cn.cjxnsb.core.config.common;


import cn.cjxnsb.common.domain.SystemStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RefreshScope
@Configuration
public class DemoConfig {

    @Value("${provider.initStatus}")
    private String initStatus;


    @Bean
    public SystemStatus systemStatus() {
        SystemStatus systemStatus = new SystemStatus();
        systemStatus.setInitStatus(initStatus);
        return systemStatus;
    }

}
