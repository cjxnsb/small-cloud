package cn.cjxnsb.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@Data
@ConfigurationProperties(prefix = "fox.swagger")
public class SwaggerProperties {

    public String basePackage;

    public String description;

}
